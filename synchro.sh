#!/bin/bash
#
#   Created by Alain Andrieux on 10/01/13.
#   Copyleft (c) 2013-2024 ADX Conseil (Alain Andrieux)
#   Creative Commons 4.0: CC BY-NC-SA
#
#		Rev. Nov. 17, 2024
#

slog=/var/log/synchro.log
rsink=$(which rsync)
optns=" -az --delete --delete-after "
src=/Volumes/data/Documents
rsvr=80.15.117.152
trg=/Volumes/spare/Marcq/
lggr="logger -i -t synchro.sh "
msg=""

report () {
	msg="${1}"
	${lggr} -p local5.notice "${msg}"
	echo "${msg}" >> ${slog}
	msg=""
}

touch ${slog}
echo " " >> ${slog}

report "Starting at `date`"

for subfold in PAO Richard accounts adfinitas compta consulting miscellaneous pdf production
do
	if [[ -d "${src}/${subfold}" ]]
	then
		report "Starting with folder ${src}/${subfold} at `date`"

		${rsink} ${optns} -e 'ssh -C -2 -p 2134' --rsync-path="${rsink}" "${src}/${subfold}" root@${rsvr}:"${trg}Documents/" 1>> ${slog} 2>&1 ; err=$?
	
		report "    Done with folder ${src}/${subfold} at `date`. Err= $err"
	else
		report "Folder ${src}/${subfold} not backed-up"
	fi
done

report "Remote backup done at `date`"

chown root:admin ${slog}
chmod 664 ${slog}

cat ${slog} | mail -s "Synchro adfinitas" alain@adfinitas.fr

exit 0
